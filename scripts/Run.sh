ssh xu943@min.ecn.purdue.edu
cd /home/dynamo/a/xzl/ece695/pub/vexpress
rsync -avxP xu943@min.ecn.purdue.edu:/home/dynamo/a/xzl/ece695/pub/vexpress ~

### Compile the kernel
cd /media/ran/Tools/OS/Assignment2/linux-linaro-stable-3.10.62-2014.12

# Setting up the cross build env vars. 
# We are building for ARM. 
export ARCH=arm
export CROSS_COMPILE=arm-linux-gnueabi-

# Make the default .config
make vexpress_defconfig

# Modify the .config, or you may do (if you have X)
# make xconfig
# 
# Enable the following options
#       CONFIG_DEVTMPFS=y
# The Extended 4 (ext4) filesystem:
#       CONFIG_EXT4_FS=y
# Support for large (2TB+) block devices and files
#       CONFIG_LBDAF=y
#

# Compile the kernel
make -j`getconf _NPROCESSORS_ONLN`
export MYKERNEL_PATH=`readlink -f arch/arm/boot/zImage`

### Run qemu
mkdir -p ~/tmp/boot/
sudo mount -o loop,offset=$((63*512)) /media/ran/Tools/OS/Assignment2/vexpress-raring_developer_20130723-408.img ~/tmp/boot

dd if=~/tmp/boot/uInitrd of=~/tmp/initrd skip=64 bs=1


qemu-system-arm \ 
-kernel ${MYKERNEL_PATH} \
-initrd ~/tmp/initrd \
-M vexpress-a9 \
-cpu cortex-a9 -serial stdio -m 1024 \
-append 'root=/dev/mmcblk0p2 rw mem=1024M raid=noautodetect console=ttyAMA0,38400n8 rootwait vmalloc=256MB devtmpfs.mount=0' \
-sd /media/ran/Tools/OS/Assignment2/vexpress-raring_developer_20130723-408.img \
-smp 4
-display none

### run on qemu
cc test.c -static -o test 
./test & cat /proc/`pgrep test`/maps

